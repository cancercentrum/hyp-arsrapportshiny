# *********************************************************************************
#   Uppföljning Hypofys -  Hormonsvikter antal, insufficiens
#   ===============================================================================
#   Urval: Period 1991-2018
#   Variabler: Antal & Någon: GH, ACTH, TSH-om, LH/FSH-om, ADH
#   Utfall: (0, 1, 2, 3, 4, 5, 9); (0, 1-2, 3-5, 9)
#
#
#   OBS: Väljer ut senaste uppf per patient & intervall
# *********************************************************************************
# Globals
NAME <- "hyp_u_svikter_antal"
diagrp_var <- "a_diagrp_kod"
sjhkod_var <- "u_uppflas"
pop <- "alla uppföljda fall."

# Skapar Dataram
df_tmp <- df_main %>%
  dplyr::arrange(pat_id, u_when, u_time) %>%
  dplyr::group_by(pat_id, a_diagrp_kod, u_when) %>%
  dplyr::slice(dplyr::n()) %>%
  dplyr::ungroup() %>%
  dplyr::filter(
    dplyr::between(lubridate::ymd(u_uppfdat), lubridate::ymd("1991-01-01"), report_end_date) &
      u_uppf == TRUE &
      a_diagrp_kod %in% c(
        "NFPA",
        "Akromegali",
        "Prolaktinom",
        "Mb Cushing",
        "TSH-om",
        "FSH/LH-om",
        "Kraniofaryngiom",
        "Rathkes cysta"
      )
  ) %>%
  add_hypofystumorer(diagrp_var) %>% 
  add_sjhdata(sjukhuskoder, sjhkod_var) %>%
  dplyr::mutate(
    outcome1 = factor(
      u_antal_svikter,
      levels = c(0, 1, 2, 3, 4, 5, 9),
      labels = c(
        "0 axlar",
        "1 axel",
        "2 axlar",
        "3 axlar",
        "4 axlar",
        "5 axlar",
        "Uppgift saknas"
      )
    ),
    outcome2 = factor(
      u_antal_svikter2,
      levels = c(0, 1, 2, 9),
      labels = c(
        "0 axlar",
        "1-2 axlar",
        "3-5 axlar",
        "Uppgift saknas"
      )
    ),
    outcome3 = factor(
      u_nagon_svikt,
      levels = c(1, 2),
      labels = c(
        "Ja",
        "Nej"
      )
    ),
    period = u_uppfar
  ) %>%
  dplyr::select(
    pat_id, 
    outcome1, 
    outcome2,
    outcome3,
    region,  
    period, 
    a_diagrp_kod,
    a_age,
    sex,
    u_when
  ) %>%
  droplevels() %>%
  as.data.frame()

# Skapar Shiny-App
hyp_u_svikter_antal <- rccShiny::rccShiny2(
  language = "sv",
  data = df_tmp,
  outcome = paste0("outcome", 1:3),
  outcomeTitle = c(
    "Antal svikter, 0-5",
    "Antal svikter, 0; 1-2; 3-5",
    "Någon svikt"
  ),
  folder = NAME,
  path = OUTPUTPATH,
  textBeforeSubtitle = paste0("Bland ", pop),
  comment = KOMMENTAR,
  description = c(

    # Om indikatorn
    paste0(
      "Antalet sviktande hormonaxlar eller andelen patienter med någon hormonaxelsvikt för vald diagnos vid vald tidpunkt."
    ),

    # Att tänka på vid tolkning
    paste0(
      "Prolaktin hämmar LH/FSH-axeln vilket förklarar att patienter med hyperprolaktinemi ofta har svikt i denna axel vid diagnos. Svikten förbättras oftast efter behandling av prolaktinomet."
    ),

    # Teknisk beskrivning
    paste0(
      "Urvalet är samtliga patienter med NFPA, akromegali, prolaktinom, Mb Cushing, TSH-om, FSH/LH-om, kraniofaryngiom och Rathkes cysta 
      som sedan 1991 är rapporterade till Hypofysregistret och som har en ögonundersökning rapporterad vid uppföljningstillfället.
      </br>
      </br>
      1 år, 0.5 - 2.5 år efter diagnos 
      </br>
      5 år, 2.5 - 7.5 år efter diagnos 
      </br>
      10 år, 7.5 - 12.5 år efter diagnos 
      </br>
      15 år, 12.5 - 17.5 år efter diagnos 
      </br>
      20 år, 17.5 - 22.5 år efter diagnos 
      </br>
      25 år, 22.5 - 27.5 år efter diagnos"
    )
  ),
  geoUnitsRegion = "region",
  periodLabel = "Uppföljningsår",
  periodDefaultStart = 1991,
  periodDefaultEnd = report_end_year,
  varOther = list(
    list(
      var = "a_diagrp_kod",
      label = "Diagnos",
      choices = levels(df_tmp$a_diagrp_kod),
      selected = "Hypofystumörer, samtliga",
      multiple = FALSE
    ),
    list(
      var = "a_age",
      label = "Ålder vid diagnos"
    ),
    list(
      var = "sex",
      label = "Kön",
      choices = levels(df_tmp$sex)
    ),
    list(
      var = "u_when",
      label = "Tidpunkt",
      choices = levels(df_tmp$u_when),
      selected = "1 år efter diagnos",
      multiple = FALSE
    )
  ),
  hideLessThanCell = 0,
  gaPath = file.path(ga_path, "ga.js")
)



# Kör Shiny-App
# cat(hyp_u_svikt_gh)
# runApp(paste0(OUTPUTPATH, "/sv/", NAME))
