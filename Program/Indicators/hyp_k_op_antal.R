# *********************************************************************************
#   Hypofys - Kirurgi (antal kirurgiska ingrepp / opår)
#   ===============================================================================
#   Urval: Period 1991-2018
#   Urval: Samtliga
#   Variabel: kirurgi
#   Utfall: Diagnoser
#
#
# *********************************************************************************
# Globals
NAME <- "hyp_k_op_antal"
diagrp_var <- "a_diagrp_kod"
sjhkod_var <- "k_vanlas"
pop <- "alla opererade fall."

# Skapar Dataram
df_tmp <- df_main %>%
  dplyr::arrange(pat_id, a_diadat, a_diagrp_kod, k_opdat, k_op) %>% 
  dplyr::distinct(pat_id, k_opdat, .keep_all = TRUE) %>% 
  dplyr::group_by(pat_id) %>%
  dplyr::mutate(k1 = dplyr::row_number(), # 1:dplyr::n()
                k2 = dplyr::n()) %>%
  dplyr::ungroup() %>%
  dplyr::filter(
    k_op == TRUE &
      dplyr::between(lubridate::ymd(k_opdat), lubridate::ymd("1991-01-01"), report_end_date) &
      a_diagrp_kod %in% c(
        "NFPA",
        "Akromegali",
        "Prolaktinom",
        "Mb Cushing",
        "TSH-om",
        "FSH/LH-om",
        "Kraniofaryngiom",
        "Rathkes cysta"
      )
  ) %>%
  add_hypofystumorer(diagrp_var) %>% 
  add_sjhdata(sjukhuskoder, sjhkod_var) %>%
  dplyr::mutate(
    outcome = factor(
      dplyr::if_else(k1 < 2, k1, 2L),
      levels = c(1, 2),
      labels = c("Primär operation", "Reoperation")
    ),
    period = k_opar
  ) %>%
  dplyr::select(
    pat_id, 
    outcome, 
    region,  
    period, 
    a_diagrp_kod,
    a_age,
    sex,
    a_micmac
  ) %>%
  droplevels() %>%
  as.data.frame()

# Skapar Shiny-App
hyp_k_op_antal <- rccShiny::rccShiny2(
  language = "sv",
  data = df_tmp,
  outcome = "outcome",
  outcomeTitle = "Antal primär- och reoperationer",
  folder = NAME,
  path = OUTPUTPATH,
  textBeforeSubtitle = paste0("Bland ", pop),
  comment = KOMMENTAR,
  description = c(

    # Om indikatorn
    paste0(
      "Antal operationer som utförts under vald tidsperiod. "
    ),

    # Att tänka på vid tolkning
    paste0(
      "Operation kan utföras under senare år än diagnosåret pga tex, självvald väntan, aktiv expektans.
       </br>
       Det kan ske en eftersläpning av inrapporterade operationer vilket kan påverka andelen som 
       rapporteras som opererade.
       </br>
       Se i övrigt under fliken ”Andel opererade”. "
    ),

    # Teknisk beskrivning
    paste0(
      "Urvalet är samtliga patienter med NFPA, akromegali, prolaktinom, Mb Cushing, TSH-om, FSH/LH-om, kraniofaryngiom och Rathkes cysta 
      som sedan 1991 har minst en operation rapporterad till registret."
    )
  ),
  geoUnitsRegion = "region",
  periodLabel = "Operationsår",
  periodDefaultStart = 1991,
  periodDefaultEnd = report_end_year,
  varOther = list(
    list(
      var = "a_diagrp_kod",
      label = "Diagnos",
      choices = levels(df_tmp$a_diagrp_kod),
      selected = "Hypofystumörer, samtliga",
      multiple = FALSE
    ),
    list(
      var = "a_age",
      label = "Ålder vid diagnos"
    ),
    list(
      var = "sex",
      label = "Kön",
      choices = levels(df_tmp$sex)
    ),
    list(
      var = "a_micmac",
      label = "Tumörstorlek",
      choices = levels(df_tmp$a_micmac)
    )
  ),
  hideLessThanCell = 0,
  gaPath = file.path(ga_path, "ga.js")
)

# Kör Shiny-App
# cat(hyp_k_op_antal)
# runApp(paste0(OUTPUTPATH, "/sv/", NAME))
