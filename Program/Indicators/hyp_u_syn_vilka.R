# *********************************************************************************
#   Uppföljning Hypofys - Synpåverkan
#   ===============================================================================
#   Urval: Period 1991-2018
#   Urval: Patienter med Synundersökning gjord
#   Variabler: u_synfaltspaverkan_str, u_visusnedsattning_str, u_opticusatrofi_str, u_ogonmuskelpares_str
#
#
#   OBS: Väljer ut senaste uppf per patient & intervall
# *********************************************************************************
# Globals
NAME <- "hyp_u_syn_vilka"
diagrp_var <- "a_diagrp_kod"
sjhkod_var <- "u_uppflas"
pop <- "alla uppföljda fall som genomfört synundersökning."

# Skapar Dataram
df_tmp <- df_main %>%
  dplyr::arrange(pat_id, u_when, u_time) %>%
  dplyr::group_by(pat_id, a_diagrp_kod, u_when) %>%
  dplyr::slice(dplyr::n()) %>%
  dplyr::ungroup() %>%
  dplyr::filter(
    dplyr::between(lubridate::ymd(u_uppfdat), lubridate::ymd("1991-01-01"), report_end_date) &
      u_uppf == TRUE &
      u_ogonundersokn_str == "Ja" &
      a_diagrp_kod %in% c(
        "NFPA",
        "Akromegali",
        "Prolaktinom",
        "Mb Cushing",
        "TSH-om",
        "FSH/LH-om",
        "Kraniofaryngiom",
        "Rathkes cysta"
      )
  ) %>%
  add_hypofystumorer(diagrp_var) %>% 
  add_sjhdata(sjukhuskoder, sjhkod_var) %>%
  dplyr::mutate(
    outcome1 = factor(u_ogonsynfalt_str),
    outcome2 = factor(u_ogonvisus_str),
    outcome3 = factor(u_ogonoptikusatrofi_str),
    outcome4 = factor(u_ogonogonmuskel_str),
    period = u_uppfar
  ) %>%
  dplyr::select(
    pat_id, 
    outcome1, 
    outcome2,
    outcome3,
    outcome4,
    region,  
    period, 
    a_diagrp_kod,
    a_age,
    sex,
    u_when
  ) %>%
  droplevels() %>%
  as.data.frame()

# Skapar Shiny-App
hyp_u_syn_vilka <- rccShiny::rccShiny2(
  language = "sv",
  data = df_tmp,
  outcome = paste0("outcome", 1:4),
  outcomeTitle = c(
    "Synfältspåverkan",
    "Visusnedsättning",
    "Optikusatrofi",
    "Ögonmuskelpares"
  ),
  folder = NAME,
  path = OUTPUTPATH,
  textBeforeSubtitle = paste0("Bland ", pop),
  comment = KOMMENTAR,
  description = c(

    # Om indikatorn
    paste0(
      "För utförlig beskrivning se under Diagnostik/synrubbingar. Synpåverkan eller hotande synpåverkan är en viktig variabel inför beslut om operation. 
      Viktigt att följa efter given behandling (operation, strålbehandling) om synrubbningar återställs eller om det tillkommer nya."
    ),

    # Att tänka på vid tolkning
    paste0(
      "Ibland kan det vara svårt att avgöra om eventuell synpåverkan beror på hypofystumören i sig eller om annan 
      ögonsjukdom kan inverka, tex katarakt eller glaukom. 
      </br>
      Uppgift om optikusatrofi och ögonmuskelpares kommenteras vanligtvis inte i ögonutlåtandet om det inte förekommer, 
      varmed ”uppgift saknas” i registret oftast betyder att det inte förekommer. 
      </br>
      Olika metoder förekommer vid synfältsundersökning som inte alltid är jämförbara. 
      </br>
      För tillförlitlig bedömning behövs upprepade synfält."
    ),

    # Teknisk beskrivning
    paste0(
      "Urvalet är samtliga patienter med NFPA, akromegali, prolaktinom, Mb Cushing, TSH-om, FSH/LH-om, kraniofaryngiom och Rathkes cysta 
      som sedan 1991 är rapporterade till Hypofysregistret och som har en ögonundersökning rapporterad vid uppföljningstillfället.
      </br>
      </br>
      1 år, 0.5 - 2.5 år efter diagnos 
      </br>
      5 år, 2.5 - 7.5 år efter diagnos 
      </br>
      10 år, 7.5 - 12.5 år efter diagnos 
      </br>
      15 år, 12.5 - 17.5 år efter diagnos 
      </br>
      20 år, 17.5 - 22.5 år efter diagnos 
      </br>
      25 år, 22.5 - 27.5 år efter diagnos"
    )
  ),
  geoUnitsRegion = "region",
  periodLabel = "Uppföljningsår",
  periodDefaultStart = 1991,
  periodDefaultEnd = report_end_year,
  varOther = list(
    list(
      var = "a_diagrp_kod",
      label = "Diagnos",
      choices = levels(df_tmp$a_diagrp_kod),
      selected = "Hypofystumörer, samtliga",
      multiple = FALSE
    ),
    list(
      var = "a_age",
      label = "Ålder vid diagnos"
    ),
    list(
      var = "sex",
      label = "Kön",
      choices = levels(df_tmp$sex)
    ),
    list(
      var = "u_when",
      label = "Tidpunkt",
      choices = levels(df_tmp$u_when),
      selected = "1 år efter diagnos",
      multiple = FALSE
    )
  ),
  hideLessThanCell = 0,
  gaPath = file.path(ga_path, "ga.js")
)

# cat(hyp_u_eye_vilka)
# runApp(paste0(OUTPUTPATH, "/sv/", NAME))
