# *********************************************************************************
#   Hypofys - Hormonsvikter antal, insufficiens
#   ===============================================================================
#   Urval: Period 1991-2018
#   Variabler: Antal & Någon: GH, ACTH, TSH-om, LH/FSH-om, ADH
#   Utfall: (0, 1, 2, 3, 4, 5, 9); (0, 1-2, 3-5, 9)
#
#
#
# *********************************************************************************
# Globals
NAME <- "hyp_a_svikter_antal"
diagrp_var <- "a_diagrp_kod"
sjhkod_var <- "a_vanlas"
pop <- "alla anmälda fall."

# Skapar Dataram
df_tmp <- df_main %>%
  dplyr::distinct(pat_id, .keep_all = TRUE) %>% 
  dplyr::filter(
    dplyr::between(lubridate::ymd(a_diadat), lubridate::ymd("1991-01-01"), report_end_date) &
      a_diagrp_kod %in% c(
        "NFPA",
        "Akromegali",
        "Prolaktinom",
        "Mb Cushing",
        "TSH-om",
        "FSH/LH-om",
        "Kraniofaryngiom",
        "Rathkes cysta",
        "Empty sella"
      )
  ) %>%
  add_hypofystumorer(diagrp_var) %>% 
  add_sjhdata(sjukhuskoder, sjhkod_var) %>%
  dplyr::mutate(
    outcome1 = factor(
      a_antal_svikter,
      levels = c(0, 1, 2, 3, 4, 5, 9),
      labels = c(
        "0 axlar",
        "1 axel",
        "2 axlar",
        "3 axlar",
        "4 axlar",
        "5 axlar",
        "Uppgift saknas"
      )
    ),
    outcome2 = factor(
      a_antal_svikter2,
      levels = c(0, 1, 2, 9),
      labels = c(
        "0 axlar",
        "1-2 axlar",
        "3-5 axlar",
        "Uppgift saknas"
      )
    ),
    outcome3 = factor(
      a_nagon_svikt,
      levels = c(1, 2),
      labels = c(
        "Ja",
        "Nej"
      )
    ),
    period = a_diaar
  ) %>%
  dplyr::select(
    pat_id, 
    outcome1, 
    outcome2, 
    outcome3, 
    region,  
    period, 
    a_diagrp_kod,     
    a_age, 
    sex
    ) %>% 
  droplevels() %>%
  as.data.frame()

# Skapar Shiny-App
hyp_a_svikter_antal <- rccShiny::rccShiny2(
  language = "sv",
  data = df_tmp,
  outcome = paste0("outcome", 1:3),
  outcomeTitle = c(
    "Antal svikter, 0-5",
    "Antal svikter, 0; 1-2; 3-5",
    "Någon svikt"
  ),
  folder = NAME,
  path = OUTPUTPATH,
  textBeforeSubtitle = paste0("Bland ", pop),
  comment = KOMMENTAR,
  description = c(

    # Om indikatorn
    paste0(
      "Antal hypofyssvikter kan ses som komplement till typ av hypofyssvikt. Ju större tumör desto större risk för flera hypofyssvikter. 
      Det är mycket ovanligt med svikt i alla fem hypofysaxlarna vid en vanlig hypofystumör men ses i registret i upp till 5-10% vid kraniofaryngiom."
    ),

    # Att tänka på vid tolkning
    paste0(
      "Se under fliken Hypofyssvikter."
    ),

    # Teknisk beskrivning
    paste0(
      "Urvalet är samtliga patienter med NFPA, akromegali, prolaktinom, Mb Cushing, TSH-om, FSH/LH-om, kraniofaryngiom och Rathkes cysta 
      som sedan 1991 är rapporterade till Hypofysregistret."
    )
  ),
  geoUnitsRegion = "region",
  periodLabel = "Diagnosår",
  periodDefaultStart = 1991,
  periodDefaultEnd = report_end_year,
  varOther = list(
    list(
      var = "a_diagrp_kod",
      label = "Diagnos",
      choices = levels(df_tmp$a_diagrp_kod),
      selected = "Hypofystumörer, samtliga",
      multiple = FALSE
    ),
    list(
      var = "a_age",
      label = "Ålder vid diagnos"
    ),
    list(
      var = "sex",
      label = "Kön",
      choices = levels(df_tmp$sex)
    )
  ),
  hideLessThanCell = 0,
  gaPath = file.path(ga_path, "ga.js")
)

# Kör Shiny-App
# cat(hyp_a_svikter_antal)
# runApp(paste0(OUTPUTPATH, "/sv/", NAME))
