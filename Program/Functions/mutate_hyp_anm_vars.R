# *********************************************************************************
#   Deriverar fram variabler inför Shiny
#   ===============================================================================
#
#
#
#
#
#
# *********************************************************************************
mutate_hyp_anm_vars <- function(x, ...) {
  x %>%

    # Synundersökning gjord ----
    # Sätter Synundersökning = Ja om respektive synrubbning något av nedanstående alternativ
    dplyr::mutate(
      a_ogonundersokn_str = dplyr::case_when(
        a_synfaltspaverkan_str %in% c("Ej bedömbar", "Ja", "Nej", "Uppgift saknas") ~ "Ja",
        a_visusnedsattning_str %in% c("Ej bedömbar", "Ja", "Nej", "Uppgift saknas") ~ "Ja",
        a_opticusatrofi_str %in% c("Ej bedömbar", "Ja", "Nej", "Uppgift saknas") ~ "Ja",
        a_ogonmuskelpares_str %in% c("Ej bedömbar", "Ja", "Nej", "Uppgift saknas") ~ "Ja",
        TRUE ~ a_ogonundersokn_str
      )
    ) %>%

    # Synrubbningar (synfaltspaverkan_str, visusnedsattning_str, opticusatrofi_str, ogonmuskelpares) ----
    # Sätter synrubbning = NA till Nej om synundersökning = NA
    dplyr::mutate_at(
      .vars = dplyr::vars(a_synfaltspaverkan_str, a_visusnedsattning_str, a_opticusatrofi_str, a_ogonmuskelpares_str),
      .funs = list(
        "factor" = ~ factor(dplyr::case_when(
          a_ogonundersokn_str == "Ja" & is.na(.) ~ "Uppgift saknas",
          a_ogonundersokn_str == "Ja" ~ .
        ),
        levels = c(
          "Ja",
          "Nej",
          "Ej bedömbar",
          "Uppgift saknas"
        )
        ),
        "01" = ~ dplyr::if_else(. == "Ja", 1L, 0L, missing = 0L)
      )
    ) %>%

    # Hormoninsufficiens ----
    # Sätter hormon = Nej om a_hypofysinsuff_str = NA & hormon = NA
    dplyr::mutate_at(
      .vars = dplyr::vars(a_gh_str, a_acth_str, a_tsh_str, a_lhfsh_str, a_adh_str),
      .funs = list(
        "factor" = ~ factor(dplyr::if_else(a_hypofysinsuff_str %in% c(NA) & is.na(.), "Nej", .),
          levels = c(
            "Ja",
            "Nej",
            "Ej bedömbart",
            "Uppgift saknas"
          )
        ),
        "01" = ~ dplyr::if_else(. == "Ja", 1L, 0L, missing = 0L)
      )
    ) %>%

    # TODO Lös namnproblematik ovan i mutate_at!?
    dplyr::mutate(
      a_synfaltspaverkan_str = a_synfaltspaverkan_str_factor,
      a_visusnedsattning_str = a_visusnedsattning_str_factor,
      a_opticusatrofi_str = a_opticusatrofi_str_factor,
      a_ogonmuskelpares_str = a_ogonmuskelpares_str_factor,
      a_gh_str = a_gh_str_factor,
      a_acth_str = a_acth_str_factor,
      a_tsh_str = a_tsh_str_factor,
      a_lhfsh_str = a_lhfsh_str_factor,
      a_adh_str = a_adh_str_factor
    ) %>%

    # TODO Lös namnproblematik ovan i mutate_at
    dplyr::select(
      -tidyselect::ends_with("_factor")
    ) %>%


    # Generell databearbetning ----
    dplyr::mutate(

      # Diagnosår
      a_diaar = as.numeric(lubridate::year(lubridate::ymd(a_diadat))),

      # Ålder - Kontinuerlig
      a_age = floor(as.numeric(lubridate::ymd(a_diadat) - lubridate::ymd(fodelsedatum)) / 365.25),

      # Ålder - Kategoriserad
      a_agegrp5 = cut(a_age, c(0, 18, 39, 59, 79, 120),
        exclude = NA,
        labels = c("<= 18", "19-39", "40-59", "60-79", "80+")
      ),

      # Kön
      # sex = factor(dplyr::case_when((as.integer(substr(persnr, 12, 12)) %% 2) == 0 ~ 2L, TRUE ~ 1L),
      #   levels = c(1, 2),
      #   labels = c("Män", "Kvinnor")
      # ),
      sex = factor(kon_value,
        levels = c(1, 2),
        labels = c("Män", "Kvinnor")
      ),

      # LKF-region för att imputera om region för sjukhus saknas
      a_region_lkf = dplyr::case_when(
        region_namn == "Region Sthlm/Gotland" ~ 1L,
        region_namn == "Region Uppsala/Örebro" ~ 2L,
        region_namn == "Region Sydöstra" ~ 3L,
        region_namn == "Region Syd" ~ 4L,
        region_namn == "Region Väst" ~ 5L,
        region_namn == "Region Norr" ~ 6L,
        TRUE ~ NA_integer_
      ),

      # Snyggar till Region
      region_namn = factor(dplyr::case_when(
        region_namn == "Region Sthlm/Gotland" ~ 1L,
        region_namn == "Region Uppsala/Örebro" ~ 2L,
        region_namn == "Region Sydöstra" ~ 3L,
        region_namn == "Region Syd" ~ 4L,
        region_namn == "Region Väst" ~ 5L,
        region_namn == "Region Norr" ~ 6L,
      ),
      levels = c(1:6),
      labels = c(
        "Stockholm-Gotland",
        "Uppsala-Örebro",
        "Sydöstra",
        "Södra",
        "Västra",
        "Norra"
      )
      ),

      # Funktioner/Diagnoser
      # ********************************
      # 1 NFPA - Non-Functioning Pituitary Adenomas (Icke hormonproducerande tumör)
      # 2	Akromegali
      # 3	Prolaktinom
      # 4	Mb Cushing
      # 5 TSH-om
      # 6	FSH/LH-om
      # 7	Kraniofaryngiom
      # 8	Rathkes cysta
      # 9	Empty sella
      # 10 Hypofysit
      # 11 Sarcoidos
      # 12 Wegener
      # 13 Metastas
      # 14 Carcinom
      # 15 Germinom
      # 16 Hamartom
      # 17 Chordom
      # 18 Angiom
      # 19 Cysta
      # 20 Meningiom
      # 21 Annan Process (Text)
      # 22 Hypofystumör & BLANK
      # 23 BLANK
      # ********************************
      a_icd10_adj = gsub(
        "Hypofystumör \\+ Multipel hormonproduktion||Hypofystumör/MEN-1|Hypofystumör|Annan process|\\(> 50% herniering\\)",
        "",
        a_icd10_str
      ),

      a_icd10_adj = dplyr::case_when(

        # Prio Hyptumörer om a_icd10_str SAKNAS
        is.na(a_icd10_str) & !is.na(a_funktion_str) ~ a_funktion_str,

        # Slask-grupp - Visas ej online
        (is.na(a_icd10_str) | a_icd10_str == "Annan") & !is.na(a_annanprocess) ~ "Övrigt (text)",

        # Hypofystumör, saknar funktion - Visas ej online
        grepl("Hypofystumör", a_icd10_str) & is.na(a_funktion_str) ~ "Hypofystumör & BLANK",

        # Saknar allt - Visas ej online
        (is.na(a_icd10_str) | a_icd10_str == "Annan") & is.na(a_funktion_str) & is.na(a_annanprocess) ~ "BLANK",

        TRUE ~ a_icd10_adj
      ),

      # Kortar ned namn
      a_funktion_adj = dplyr::if_else(
        a_funktion_str == "Icke hormonproducerande E23 6",
        "NFPA",
        substr(a_funktion_str, 1, stringr::str_length(a_funktion_str) - 6),
        missing = NA_character_
      ),
      a_diagrp = stringr::str_trim(dplyr::coalesce(a_funktion_adj, a_icd10_adj), side = c("both")),

      # TEMP-Fix: Sydöstra 1991-2015 FSH/LH-om --> NFPA - Finns online mall för detta.
      a_diagrp = dplyr::if_else(region_namn %in% c("Region Sydöstra") & dplyr::between(a_diaar, 1991, 2015) & a_diagrp %in% c("FSH/LH-om"), "NFPA", a_diagrp),

      # Slutgiltig diagnos-variabel
      a_diagrp_kod = factor(dplyr::case_when(
        a_diagrp %in% c(
          "NFPA",
          "Akromegali",
          "Prolaktinom",
          "Mb Cushing",
          "TSH-om",
          "FSH/LH-om",
          "Kraniofaryngiom",
          "Rathkes cysta",
          "Empty sella"
        ) ~ a_diagrp,
        a_diagrp %in% c(
          "Hypofysit",
          "Sarcoidos",
          "Wegener",
          "Metastas",
          "Carcinom",
          "Germinom",
          "Hamartom",
          "Chordom",
          "Angiom",
          "Cysta",
          "Meningiom",
          "Granularcellstumör",
          "Hypofysabscess",
          "Pituicytom"
        ) ~ "Övriga processer"
      ),
      levels = c(
        "NFPA",
        "Akromegali",
        "Prolaktinom",
        "Mb Cushing",
        "TSH-om",
        "FSH/LH-om",
        "Kraniofaryngiom",
        "Rathkes cysta",
        "Empty sella",
        "Övriga processer"
      )
      ),

      # # Röntgen (MR/CT) vid diagnos
      # a_mr_ct = dplyr::if_else(
      #   (a_mrct_str %in% c("CT", "MR") |
      #     a_ct_str == "Ja" |
      #     grepl("Ja", a_mr_str) |
      #     a_radioresultat_str %nin% c("Uppgift saknas", NA) |
      #     !is.na(a_storlekv) |
      #     a_suprasellar_str %nin% c("Uppgift saknas", NA) |
      #     a_parasellarv_str %nin% c("Uppgift saknas", NA) |
      #     a_parasellarh_str %nin% c("Uppgift saknas", NA) |
      #     a_cystisitet_str %nin% c("Uppgift saknas", NA)),
      #   TRUE, FALSE,
      #   missing = FALSE
      # ),

      # Storlek
      a_micmac = factor(dplyr::case_when(
        a_radioresultat_str == "Makroadenom (>= 10 mm)" ~ "Makro (>= 10 mm)",
        a_radioresultat_str == "Mikroadenom (< 10 mm)" ~ "Mikro (< 10 mm)",
        a_radioresultat_str %in% c("Ej bedömbart", "Ej synlig", "Uppgift saknas") ~ a_radioresultat_str,
        is.na(a_radioresultat_str) ~ "Uppgift saknas"
      ),
      levels = c(
        "Makro (>= 10 mm)",
        "Mikro (< 10 mm)",
        "Ej bedömbart",
        "Ej synlig",
        "Uppgift saknas"
      )
      ),

      # synrubbningar ----
      # Räknar antal synrubbningar
      a_antal_synfel = (a_synfaltspaverkan_str_01 + a_visusnedsattning_str_01 + a_opticusatrofi_str_01 + a_ogonmuskelpares_str_01),
      a_antal_synfel = dplyr::if_else(
        a_synfaltspaverkan_str == "Uppgift saknas" &
          a_visusnedsattning_str == "Uppgift saknas" &
          a_opticusatrofi_str == "Uppgift saknas" &
          a_ogonmuskelpares_str == "Uppgift saknas",
        9L,
        a_antal_synfel
      ),

      # Någon synrubbning
      a_nagot_synfel = dplyr::case_when(
        dplyr::between(a_antal_synfel, 1, 4) ~ 1L,
        TRUE ~ 2L
      ),

      # Hormoninsufficiens ----
      # Räknar antal axlar
      a_antal_svikter = (a_gh_str_01 + a_acth_str_01 + a_tsh_str_01 + a_lhfsh_str_01 + a_adh_str_01),
      a_antal_svikter = dplyr::if_else(
        a_gh_str == "Uppgift saknas" &
          a_acth_str == "Uppgift saknas" &
          a_tsh_str == "Uppgift saknas" &
          a_lhfsh_str == "Uppgift saknas" &
          a_adh_str == "Uppgift saknas",
        9L,
        a_antal_svikter
      ),

      # Antal axlar; 0, 1-2, 3-5, 9
      a_antal_svikter2 = dplyr::case_when(
        a_antal_svikter == 0 ~ 0L,
        between(a_antal_svikter, 1, 2) ~ 1L,
        between(a_antal_svikter, 3, 5) ~ 2L,
        a_antal_svikter == 9 ~ 9L
      ),

      # Någon svikt
      a_nagon_svikt = dplyr::case_when(
        dplyr::between(a_antal_svikter2, 1, 2) ~ 1L,
        TRUE ~ 2L
      ),

      # Multidisciplinär Konferens (MDT/MDK)
      a_mdk = as.logical(dplyr::if_else(a_multikonf_str %in% c("Ja"), TRUE, FALSE, missing = FALSE)),

      # Sjukskrivningsgrad
      a_sjukgrad = factor(a_sjukskrivngrad_str,
        levels = c(
          "0%",
          "25%",
          "50%",
          "75%",
          "100%",
          "Avtals/ålderspensionär",
          "Sjukpensionär",
          "Uppgift saknas"
        )
      ),

      # Sjukskrivningsgrad2
      a_sjukgrad2 = factor(dplyr::case_when(
        a_sjukskrivngrad_str %in% c("0%") ~ a_sjukskrivngrad_str,
        a_sjukskrivngrad_str %in% c("25%", "50%", "75%", "100%") ~ "25-100%",
        a_sjukskrivngrad_str %in% c("Avtals/ålderspensionär", "Sjukpensionär", "Uppgift saknas") ~ a_sjukskrivngrad_str
      ),
      levels = c(
        "0%",
        "25-100%",
        "Avtals/ålderspensionär",
        "Sjukpensionär",
        "Uppgift saknas"
      )
      ),

      # Farmakologisk behandling
      a_farma = dplyr::case_when(

        # Somatostatin analog: Oktreotid/lanreotid, Pasireotid
        !is.na(a_medsomaanalog) |
          !is.na(a_medoktreotidlanreo) |
          !is.na(a_medpasireotid) |

          # Dopamin agonist: Bromokriptin, Kabergolin, Kinagolid
          !is.na(a_meddopaminagonist) |
          !is.na(a_medbromokriptin) |
          !is.na(a_medkabergolin) |
          !is.na(a_medkinagolid) |

          # Pegvisomant, Ketokonazol, Metyrapon & Temozolomide
          !is.na(a_medpegvisomant) |
          !is.na(a_medketokonazol) |
          !is.na(a_medmetyrapon) |
          !is.na(a_medtemozolomide) |

          # Annat - Fritext & ATC-koder
          !is.na(a_medannat) |
          !is.na(a_medannatvad) |
          !is.na(a_medatc) ~ "Ja",
        TRUE ~ "Nej"
      ),

      # Ledtider - diagnos till registrering för de som har positiva ledtider
      ledtid_dia_reg = as.numeric(lubridate::ymd(a_rappdat) - lubridate::ymd(a_diadat)),
      ledtid_dia_reg = dplyr::if_else(ledtid_dia_reg < 0, NA_real_, ledtid_dia_reg, ledtid_dia_reg),
    )
}
