# Kod för interaktiva rapporter från Svenska Hypofysregistret

## Syfte
Skapa interaktiva grafer för publikation på https://statistik.incanet.se/Hypofys/Rapport/

## Begränsningar/beroende
- Beror av externt dataset med sjukhuskoder och rådata från registret som inte finns i Bitbucket.
- Nödvändigt R-bibliotek: [rccShiny](https://cancercentrum.bitbucket.io/rccshiny). 

## Användning
Kör **main.R** för att skapa en applikation per indikator.

